/*
~~~Batalla Naval~~~
  Proyecto Final
 Diego Pe�a Benitez
       1AV7
*/

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>


int fila;
int columna;
int barco;
int matrizjugador[7][7];
int matrizcomputadora[7][7];
int lugarvacio=0;
int posicionbarco=1;
int barcohundido=2;
int lugarataque=3;
int coordenadaXnum;
int coordenadaYnum;
int aux;
int probcomputadora;
int barcoshundidosjug=0;
int barcoshundidoscom=0;
float dificultad=0.20;
char coordenadaX;
char coordenadaY;
char nombrejugador[15];


void PantallaCompletaEnVentana();
void nombre();
void simbologia();
void generartablerojugador();
void generartablerocompu();
void colocarbarcos();
void disparosjugadorcomputadora();

void color(int c, int b){
  HANDLE  hConsole;
  hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
  FlushConsoleInputBuffer(hConsole);
  SetConsoleTextAttribute(hConsole, c+16*b);
}
enum{BLACK=0,BLUE=1,GREEN=2,CYAN=3,RED=4,PURPLE=5,YELLOW=6,WHITE=7,GRAY=8,BBLUE=9,BGREEN=10,BCYAN=11,BRED=12,BPURPLE=13,BYELLOW=14,BWHITE=15};
void gotoxy(int x, int y){
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);

}
void printxy(int x,int y,char c){
    gotoxy(x,y);printf("%c",c);
}
void printLine(int x1, int y1, int x2, int y2, char c){
    float dy, dx, ady, adx;
    int i;
    dx=x2-x1;adx=dx<0?-1*dx:dx;
    dy=y2-y1;ady=dy<0?-1*dy:dy;
    float mayor= (adx>ady?adx:ady);
    for(i=0;i<=mayor;i++){
        printxy(x1+i*(dx/mayor),y1+i*(dy/mayor),c);
    }
}
void printCuadro(int x1,int y1,int x2,int y2){
    printLine(x1,y1,x2,y1,205);
    printLine(x2,y1,x2,y2,186);
    printLine(x2,y2,x1,y2,205);
    printLine(x1,y2,x1,y1,186);
    printxy(x1,y1,201);
    printxy(x2,y1,187);
    printxy(x1,y2,200);
    printxy(x2,y2,188);

}
void PantallaCompletaEnVentana(){

    ShowWindow(GetConsoleWindow(), SW_MAXIMIZE);
}
void nombre(){
    gotoxy(10,5);color(BYELLOW,BLACK);printf("Ingresa tu nombre de jugador: ");
    color(BWHITE,BLACK);
    gets(nombrejugador);
    system("cls");
}
void simbologia(){
    color(BYELLOW,BLACK);
    printCuadro(133,15,161,24);
    gotoxy(142,17);printf("SIMBOLOGIA");
    gotoxy(135,19);color(BWHITE,BLACK);printf("~");gotoxy(137,19);color(BWHITE,BLACK);printf("= Espacio en el tablero");
    gotoxy(135,20);color(CYAN,BLACK);printf("#");gotoxy(137,20);color(BWHITE,BLACK);printf("= Posicion de un barco");
    gotoxy(135,21);color(BPURPLE,BLACK);printf("O");gotoxy(137,21);color(BWHITE,BLACK);printf("= Lugar de disparo");
    gotoxy(135,22);color(BRED,BLACK);printf("X");color(BWHITE,BLACK);gotoxy(137,22);printf("= Barco hundido");

}
void generartablerojugador(){
    color(BLUE,BLACK);
    gotoxy(33,0);printf("%s",nombrejugador);
    color(BWHITE,BLACK);
    printf("\n");

    for(fila=1;fila<=6;fila++) // Imprime el tablero del jugador
    {

        printf("\n\n\n\t\t");
        for(columna=1;columna<=6;columna++)
        {
            if(matrizjugador[fila][columna]==lugarataque)
            {

                color(BPURPLE,BLACK);
                printf("O\t");
                color(BWHITE,BLACK);

            }
            else
            {
                if(matrizjugador[fila][columna]==barcohundido)
                {
                    color(BRED,BLACK);
                    printf("X\t");
                    color(BWHITE,BLACK);
                }
                else
                {
                    if(matrizjugador[fila][columna]==posicionbarco)
                    {
                        color(CYAN,BLACK);
                        printf("#\t");
                        color(BWHITE,BLACK);

                    }
                    else
                    {
                        printf("~\t");
                    }
                }
            }
        }


}


    color(BGREEN,BLACK);
    gotoxy(12,4);printf("1");
    gotoxy(12,7);printf("2");
    gotoxy(12,10);printf("3");
    gotoxy(12,13);printf("4");
    gotoxy(12,16);printf("5");
    gotoxy(12,19);printf("6");
    gotoxy(16,2);printf("1");
    gotoxy(24,2);printf("2");
    gotoxy(32,2);printf("3");
    gotoxy(40,2);printf("4");
    gotoxy(48,2);printf("5");
    gotoxy(56,2);printf("6");
    color(BWHITE,BLACK);
    printLine(12,21,56,21,205);
    color(BLUE,BLACK);
    printCuadro(68,1,89,5);
    color(BWHITE,BLACK);
    gotoxy(70,3);printf("Barcos Hundidos: %d",barcoshundidoscom);
    simbologia();

}
void generartablerocompu(){
    color(RED,BLACK);
    gotoxy(29,22);printf("Computadora");
    color(BWHITE,BLACK);
    printf("\n");

    for (fila=1;fila<=6;fila++){

        printf("\n\n\n\t\t");
        for(columna=1;columna<=6;columna++){
            if(matrizcomputadora[fila][columna]==lugarataque)
            {

                color(BPURPLE,BLACK);
                printf("O\t");
                color(BWHITE,BLACK);

            }
            else
            {
                if(matrizcomputadora[fila][columna]==barcohundido)
                {
                    color(BRED,BLACK);
                    printf("X\t");
                    color(BWHITE,BLACK);

                }
                else
                {
                    printf("~\t");
                }
            }
        }

    }


    color(BGREEN,BLACK);
    gotoxy(12,26);printf("1");
    gotoxy(12,29);printf("2");
    gotoxy(12,32);printf("3");
    gotoxy(12,35);printf("4");
    gotoxy(12,38);printf("5");
    gotoxy(12,41);printf("6");
    gotoxy(16,24);printf("1");
    gotoxy(24,24);printf("2");
    gotoxy(32,24);printf("3");
    gotoxy(40,24);printf("4");
    gotoxy(48,24);printf("5");
    gotoxy(56,24);printf("6");
    color(RED,BLACK);
    printCuadro(68,38,89,42);
    color(BWHITE,BLACK);
    gotoxy(70,40);printf("Barcos Hundidos: %d",barcoshundidosjug);

}

void colocarbarcos(){

    for(fila=1;fila<=6;fila++){
        for(columna=1;columna<=6;columna++)
        {
            matrizjugador[fila][columna]=lugarvacio;
            matrizcomputadora[fila][columna]=lugarvacio;
        }
    }

    srand(time(NULL));
    for(barco=1;barco<=5;barco++) // Distribuye los barcos
    {
        generartablerojugador();
        printf("\n");
        generartablerocompu();

        fila=1+rand()%6; // Distribuye los barcos oponentes
        columna=1+rand()%6;
        while(matrizcomputadora[fila][columna]==posicionbarco)
        {
            fila=1+rand()%6;
            columna=1+rand()%6;
        }
        matrizcomputadora[fila][columna]=posicionbarco; // La posicion de los barcos valdra 1

        gotoxy(65,18); printf("Ingresa la coordenada horizontal del barco %d = ", barco);
        scanf("%s", &coordenadaX);
        while(coordenadaX<49 || coordenadaX>54)
        {
            gotoxy(65,19);printf("Escoje un valor valido ( 1 al 6 )= ");
            scanf("%s", &coordenadaX);
            printf("\n");

        }

        gotoxy(65,22);printf("Ingresa la coordenada vertical del barco %d = ", barco);
        scanf("%s", &coordenadaY);
        while(coordenadaY<49 || coordenadaY>54)
        {
            gotoxy(65,23);printf("Escoje un valor valido ( 1 al 6 ) = ");
            scanf("%s", &coordenadaY);
            printf("\n");
        }
        coordenadaXnum=coordenadaX-'0';
        coordenadaYnum=coordenadaY-'0';

        if(matrizjugador[coordenadaYnum][coordenadaXnum]==posicionbarco)
        {
            gotoxy(65,26);printf("Ese valor ya existe");
            getchar();
            barco=barco-1;
        }
        matrizjugador[coordenadaYnum][coordenadaXnum]=posicionbarco;

        system("cls");

    }


}
void disparosjugadorcomputadora(){

    gotoxy(65,16);printf("ES TU TURNO");
    gotoxy(65,17);printf("Ingresa las coordenadas que quieres atacar");
    gotoxy(65,18);printf("La coordenada horizontal (1 al 6) es: ");
    scanf("%s",&coordenadaX);
    while(coordenadaX<49 || coordenadaX>54)
        {
            gotoxy(65,19);printf("Escoje un valor valido ( 1 al 6 )= ");
            scanf("%s", &coordenadaX);
            printf("\n");

        }
    gotoxy(65,21);printf("La coordenada vertical (1 al 6) es: ");
    scanf("%s",&coordenadaY);
    while(coordenadaY<49 || coordenadaY>54)
        {
            gotoxy(65,22);printf("Escoje un valor valido ( 1 al 6 ) = ");
            scanf("%s", &coordenadaY);
            printf("\n");
        }

        coordenadaXnum=coordenadaX-'0';
        coordenadaYnum=coordenadaY-'0';

        aux=matrizcomputadora[coordenadaYnum][coordenadaXnum];
        matrizcomputadora[coordenadaYnum][coordenadaXnum]=lugarataque;
        system("cls");
        generartablerojugador();
        printf("\n");
        generartablerocompu();
        matrizcomputadora[coordenadaYnum][coordenadaXnum]=aux;

        if(matrizcomputadora[coordenadaYnum][coordenadaXnum]==posicionbarco){
            matrizcomputadora[coordenadaYnum][coordenadaXnum]=barcohundido;
            barcoshundidoscom=barcoshundidoscom+1;
            gotoxy(65,16);printf("HAZ HUNDIDO UN BARCO");

        }
        else{
            matrizcomputadora[coordenadaYnum][coordenadaXnum]=lugarataque;
            gotoxy(65,16);printf("LA BALA SE FUE AL AGUA");
        }
        gotoxy(65,17);system("PAUSE");
        system("cls");

        if(barcoshundidoscom==5){
            system("cls");
            gotoxy(65,10);color(BLUE,BLACK);printf("VICTORIA");
            color(BWHITE,BLACK);
            getchar();
            gotoxy(0,20);
        }
        else{
        generartablerojugador();
        printf("\n");
        generartablerocompu();


        gotoxy(65,25);printf("TURNO DE LA COMPUTADORA");
        srand(time(NULL));
        probcomputadora=rand()%6;
        if(probcomputadora>dificultad){
            fila=1+rand()%6;
            columna=1+rand()%6;
            while(matrizjugador[fila][columna]==barcohundido){
                fila=1+rand()%6;
                columna=1+rand()%6;
            }
            aux=matrizjugador[fila][columna];
        }
        else{
            while(matrizjugador[fila][columna]==barcohundido || matrizjugador[fila][columna]!=posicionbarco){
                fila=1+rand()%6;
                columna=1+rand()%6;
            }
            aux=matrizjugador[fila][columna];
        }

        matrizjugador[fila][columna]=lugarataque;
        coordenadaYnum=fila;
        coordenadaXnum=columna;
        system("cls");
        generartablerojugador();
        printf("\n");
        generartablerocompu();
        matrizjugador[coordenadaYnum][coordenadaXnum]=aux;

        if(matrizjugador[coordenadaYnum][coordenadaXnum]==posicionbarco){
            gotoxy(65,25);printf("LA COMPUTADORA HA HUNDIDO UN BARCO");
            matrizjugador[coordenadaYnum][coordenadaXnum]=barcohundido;
            barcoshundidosjug=barcoshundidosjug+1;
        }
        else{
            matrizjugador[coordenadaYnum][coordenadaXnum]=lugarataque;
            gotoxy(65,25);printf("LA BALA DE LA COMPUTADORA SE FUE AL AGUA");

        }


        if(barcoshundidosjug==5){
            system("cls");
            gotoxy(65,10);color(RED,BLACK);printf("DERROTA");
            color(BWHITE,BLACK);
            getchar();
            gotoxy(0,20);
        }

        gotoxy(65,26);system("PAUSE");
        gotoxy(65,26);system("cls");
        }

        if(barcoshundidoscom==5 || barcoshundidosjug==5){
           barcoshundidoscom=5;
           barcoshundidosjug=5;

        }

}


int main(){

    int opcionmenu;
    int cjugar;
    int volverjugar;
    char letra=164;

    PantallaCompletaEnVentana();
    nombre();

    color(YELLOW,BLACK);
    gotoxy(57,10);printf("BIENVENIDO A BATALLA NAVAL, %s ",nombrejugador);
    color(BWHITE,BLACK);
    gotoxy(60,14);printf("1. Jugar");
    gotoxy(60,16);printf("2. Como jugar");
    gotoxy(60,18);printf("3. Salir");
    color(GREEN,BLACK);
    gotoxy(60,35);printf("Por: Diego Pe a Benitez");
    gotoxy(73,35);printf("%c",letra);
    color(CYAN,BLACK);
    gotoxy(60,21);printf("Seleccione la opcion: ");
    color(BWHITE,BLACK);
    scanf("%d",&opcionmenu);


    switch (opcionmenu){
    case 1:
        do{
        barcoshundidoscom=0;
        barcoshundidosjug=0;
        system("cls");
        colocarbarcos();
        do{
        system("cls");
        generartablerojugador();
        printf("\n");
        generartablerocompu();
        printf("\n");
        disparosjugadorcomputadora();
        }
        while(barcoshundidoscom<5 || barcoshundidosjug<5);
        color(GREEN,BLACK);
        gotoxy(60,10);printf("Desea volver a jugar:");
        color(BWHITE,BLACK);
        gotoxy(60,12);printf("1. Si");
        gotoxy(60,13);printf("2. No");
        gotoxy(60,15);color(CYAN,BLACK);printf("Escoge la opcion:");
        color(BWHITE,BLACK);
        scanf("%d",&volverjugar);
        gotoxy(0,20);
        }
        while(volverjugar==1);
        break;

    case 2:
        system("cls");
        gotoxy(60,6);color(BRED,BLACK);printf("COMO JUGAR BATALLA NAVAL");
        color(BWHITE,BLACK);
        gotoxy(20,9);printf("1. Coloca tus 5 barcos en el tablero, comenzando por designar la coordenada horizontal y despues la vertical. ");
        gotoxy(20,10);printf("2. Una vez que coloques tus barcos, comenzara la fase de ataque");
        gotoxy(20,11);printf("3. Durante tu turno ingresa la coordenada que deseas atacar, primero la parte horizontal y despues la vertical de la coordenada.");
        gotoxy(20,12);printf("4. Si aciertas hundiras un barco, si no aciertas la bala se ira al agua.");
        gotoxy(20,13);printf("5. Una vez que termine tu turno, sera el de la computadora y tratara de hundir uno de tus barcos.");
        gotoxy(20,14);printf("6. Ganara quien logre hundir primero los 5 barcos del otro.");
        gotoxy(20,20);color(GREEN,BLACK);printf("Desea jugar: ");
        color(BWHITE,BLACK);
        gotoxy(20,22);printf("1. Si ");
        gotoxy(20,23);printf("2. No ");
        gotoxy(20,25);color(CYAN,BLACK);printf("Escoge la opcion: ");
        color(BWHITE,BLACK);
        scanf("%d",&cjugar);
        if(cjugar==1){
        do{
            barcoshundidoscom=0;
            barcoshundidosjug=0;
            system("cls");
            colocarbarcos();
            do{
                system("cls");
                generartablerojugador();
                printf("\n");
                generartablerocompu();
                printf("\n");
                disparosjugadorcomputadora();
            }
        while(barcoshundidoscom<5 || barcoshundidosjug<5);
        color(GREEN,BLACK);
        gotoxy(60,10);printf("Desea volver a jugar:");
        color(BWHITE,BLACK);
        gotoxy(60,12);printf("1. Si");
        gotoxy(60,13);printf("2. No");
        gotoxy(60,15);color(CYAN,BLACK);printf("Escoge la opcion:");
        color(BWHITE,BLACK);
        scanf("%d",&volverjugar);
        gotoxy(0,20);
        }
        while(volverjugar==1);
        }
        else{
            system("cls");
            gotoxy(60,12);printf("GRACIAS POR JUGAR");
            gotoxy(60,35);
        }

        break;

    case 3:
        system("cls");
        gotoxy(60,12);printf("GRACIAS POR JUGAR");
        gotoxy(60,35);
        break;

    default:
        gotoxy(60,22);printf("Opcion invalida");
        gotoxy(0,37);
        break;
    }

    return 0;
}

